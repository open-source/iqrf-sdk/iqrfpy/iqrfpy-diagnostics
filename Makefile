.PHONY: docs

build:
	python3 -m pip install --upgrade build
	python3 -m build

docs:
	rm -rf docs
	pdoc -d google -o docs iqd_diagnostics/

install:
	python3 -m pip install .

upload-test:
	python3 -m pip install --upgrade twine
	python3 -m twine upload --repository testpypi dist/*

upload:
	python3 -m pip install --upgrade twine
	python3 -m twine upload dist/*

clean:
	rm -rf build dist iqrfpy_iqd_diagnostics.egg-info .pytest_cache htmlcov docs
	find . -type d -name  "__pycache__" -exec rm -r {} +

codestyle:
	pycodestyle iqd_diagnostics
	pydocstyle iqd_diagnostics

lint:
	pylint iqd_diagnostics

pytest:
	python3 -m pytest -v tests

test: pytest codestyle lint

cov-html:
	python3 -m pytest --cov=iqd_diagnostics --cov-report=term-missing --cov-report=html

cov-xml:
	python3 -m pytest --cov=iqd_diagnostics --cov-report=term-missing --cov-report=xml

