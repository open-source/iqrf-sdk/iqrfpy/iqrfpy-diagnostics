import random
from typing import List
import unittest

from parameterized import parameterized
from iqd_diagnostics import (
    DiagnosticsResolver,
    InvalidDiagnosticsDataLengthError,
    UnsupportedDiagnosticsVersionError,
)


class ObjectsTestCase(unittest.TestCase):

    @parameterized.expand([
        [1]
    ])
    def test_diagnostics_resolver_get_by_version_ok(self, version: int):
        DiagnosticsResolver.get_diagnostics_by_version(version=version)

    @parameterized.expand([
        [5]
    ])
    def test_diagnostics_resolver_get_by_version_unknown(self, version: int):
        with self.assertRaises(UnsupportedDiagnosticsVersionError):
            DiagnosticsResolver.get_diagnostics_by_version(version=version)

    @parameterized.expand([
        ['Version 1', 1, [0xD1] + random.sample(range(0, 255), 49)]
    ])
    def test_diagnostics_version_validate_ok(self, _, version: int, data: List[int]):
        diag_version = DiagnosticsResolver.get_diagnostics_by_version(version=version)
        diag_version.validate(data)

    @parameterized.expand([
        ['Version 1', 1, [0xD1] + random.sample(range(0, 255), 40)]
    ])
    def test_diagnostics_version_validate_invalid(self, _, version: int, data: List[int]):
        diag_version = DiagnosticsResolver.get_diagnostics_by_version(version=version)
        with self.assertRaises(InvalidDiagnosticsDataLengthError):
            diag_version.validate(data)
