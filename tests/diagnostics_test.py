import random
from typing import List
import unittest

from parameterized import parameterized
from iqd_diagnostics import (
    DIAGNOSTICS_HEADER_VALUE,
    InvalidDiagnosticsDataError,
    InvalidDiagnosticsDataLengthError,
    IqdDiagnostics,
    UnsupportedDiagnosticsVersionError,
)
from iqd_diagnostics.iqd_diagnostics import seconds_to_duration
from iqrfpy.objects import SensorData
from iqrfpy.utils.quantity_data import Temperature


class DiagnosticsTestCase(unittest.TestCase):

    def setUp(self):
        self.raw_data = [DIAGNOSTICS_HEADER_VALUE, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                         0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                         0x00, 0x00, 0x00, 0xA2, 0x39, 0x00, 0x00, 0xFF, 0x7F, 0x01, 0x80, 0xC8, 0x00, 0x00, 0x00,
                         0x00, 0x00, 0x00, 0x00, 0xFF, 0x7F, 0x00, 0x00]

    @parameterized.expand([
        ['Empty', None],
        ['Invalid diagnostics header', [0x5A] + random.sample(range(0, 255), 49)],
    ])
    def test_init_invalid(self, _, data: List[int]):
        with self.assertRaises(InvalidDiagnosticsDataError):
            IqdDiagnostics(data=data)

    @parameterized.expand([
        ['Unsupported diagnostics version', [DIAGNOSTICS_HEADER_VALUE, 0x02] + random.sample(range(0, 255), 48)],
    ])
    def test_init_unsupported(self, _, data: List[int]):
        with self.assertRaises(UnsupportedDiagnosticsVersionError):
            IqdDiagnostics(data=data)

    @parameterized.expand([
        ['Version 1', [DIAGNOSTICS_HEADER_VALUE, 0x01] + random.sample(range(0, 255), 40)],
    ])
    def test_init_invalid_len(self, _, data: List[int]):
        with self.assertRaises(InvalidDiagnosticsDataLengthError):
            IqdDiagnostics(data=data)

    @parameterized.expand([
        [[0x01], [True] + [False] * 7],
        [[0xFF], [True] * 8],
        [[0xF0, 0x0F], [False] * 4 + [True] * 8 + [False] * 4],
    ])
    def test_get_flags(self, data: List[int], expected: List[bool]):
        self.assertEqual(
            IqdDiagnostics.get_flags(data),
            expected
        )

    @parameterized.expand([
        [
            SensorData(
                sensor_type=Temperature.type,
                name=Temperature.name,
                short_name=Temperature.short_name,
                unit=Temperature.unit,
                frc_commands=Temperature.frc_commands,
                decimal_places=Temperature.decimal_places,
                index=0,
                value=22.5,
            ),
            '22.5 ˚C'
        ]
    ])
    def test_format_measurement(self, data: SensorData, expected: str):
        self.assertEqual(
            IqdDiagnostics._format_measurement(data),
            expected
        )

    def test_to_string(self):
        diagnostics_data = IqdDiagnostics(data=self.raw_data)
        diagnostics_data.to_string()

    @parameterized.expand([
        ['0s', 0, '0 seconds'],
        ['50s', 50, '50 seconds'],
        ['59s', 59, '59 seconds'],
        ['60s', 60, '1 minute'],
        ['5m2s', 5 * 60 + 2, '5 minutes, 2 seconds'],
        ['10m1s', 10 * 60 + 1, '10 minutes, 1 second'],
        ['59m', 59 * 60, '59 minutes'],
        ['60m', 60 * 60, '1 hour'],
        ['1h5m10s', 65 * 60 + 10, '1 hour, 5 minutes, 10 seconds'],
        ['2h5s', 2 * 60 * 60 + 5, '2 hours, 5 seconds'],
        ['23h5m', 23 * 60 * 60 + 5 * 60, '23 hours, 5 minutes'],
        ['24h', 60 * 60 * 24, '1 day'],
        ['2d5h2m1s', 60 * 60 * 24 * 2 + 60 * 60 * 5 + 60 * 2 + 1, '2 days, 5 hours, 2 minutes, 1 second'],
        ['1w', 60 * 60 * 24 * 7, '1 week'],
        ['5w10m', 60 * 60 * 24 * 7 * 5 + 10 * 60, '5 weeks, 10 minutes'],
        ['1y', 60 * 60 * 24 * 365, '1 year'],
        ['5y1h10s', 60 * 60 * 24 * 365 * 5 + 60 * 60 + 10, '5 years, 1 hour, 10 seconds']
    ])
    def test_seconds_to_duration(self, _, value: int, expected: str):
        self.assertEqual(
            seconds_to_duration(seconds=value),
            expected
        )
