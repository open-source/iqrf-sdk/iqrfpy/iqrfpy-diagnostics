## Changelog

### Version: 0.1.3

Release date: 11.06.2024

#### Changes

- Swapped order of last and archive columns in errors table

### Version: 0.1.2

Release date: 18.04.2024

#### Changes

- Module renamed from `diagnostics` to `iqd_diagnostics`

### Version: 0.1.1 (REMOVED)

Release date: 16.04.2024

#### Changes

- Updated docs, links

### Version: 0.1.0 (UNRELEASED)

#### Changes

- Initial implementation
